import React, {useEffect} from 'react';
import Alarm from 'react-native-alarm-manager';

function App(props: any): JSX.Element {
  const alarm = {
    alarm_time: '03:23:00', //hora definida HH:MM:SS
    alarm_title: 'title',
    alarm_text: 'text',
    alarm_sound: 'bliss', //Som vem da pasta raw, nao e necessario colocar o .mp3
    alarm_icon: 'like', //Icone vem da pasta drawble, nao e necessario colocar o .png
    alarm_sound_loop: true,
    alarm_vibration: true,
    alarm_noti_removable: true,
    alarm_activate: true,
  };

  //cria o alarme
  Alarm.schedule(
    alarm,
    success => console.log(success), //aqui so tem um log, mas pode abrir a função e fazer qualquer coisa
    fail => console.log(fail),
  );

  //busca e lista os alarmes ativos
  Alarm.searchAll(
    success => console.log(success), //success é a lista
    fail => console.log(fail),
  );

  useEffect(() => {
    console.log(props);
  }, [props]);

  return <></>;
}

export default App;
