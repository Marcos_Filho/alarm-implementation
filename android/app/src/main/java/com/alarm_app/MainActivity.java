package com.alarm_app;


import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;

import androidx.annotation.Nullable;


import com.baekgol.reactnativealarmmanager.AlarmModule;
import com.baekgol.reactnativealarmmanager.util.BootReceiver;

import com.facebook.react.ReactActivity;
import com.facebook.react.ReactActivityDelegate;
import com.facebook.react.defaults.DefaultNewArchitectureEntryPoint;
import com.facebook.react.defaults.DefaultReactActivityDelegate;

public class MainActivity extends ReactActivity {

  /**
   * Returns the name of the main component registered from JavaScript. This is used to schedule
   * rendering of the component.
   */
  @Override
  protected String getMainComponentName() {
    return "alarm_app";
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    ComponentName receiver = new ComponentName(this, BootReceiver.class);
    PackageManager packageManager = this.getPackageManager();

    packageManager.setComponentEnabledSetting(
      receiver,
      PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
      PackageManager.DONT_KILL_APP
    );
  }

  @Override
  protected ReactActivityDelegate createReactActivityDelegate() {
    return new ReactActivityDelegate(this, getMainComponentName()) {
      @Nullable
      @Override
      protected Bundle getLaunchOptions() {
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        if (intent.getBooleanExtra("notiRemovable", true)) AlarmModule.stop(
          this.getContext()
        );

        return bundle;
      }
    };
  }
}

